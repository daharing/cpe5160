#ifndef _Outputs_H
#define _Outputs_H

#include "Main.H"
#include "PORT.H"

/***********************************************************************
DESC:  Clears the specified bits on Port 2 to switch on LEDS.
INPUT: 8-bit pattern with '1' bits for the LEDS to be switched on
RETURNS: Nothing
CAUTION: LEDS must be connected to Port 2 and be active low
************************************************************************/
void LED_On(uint8_t LED_bits);

/***********************************************************************
DESC:  Set the specified bits on Port 2 to switch off LEDS
INPUT: 8-bit pattern with '1' bits for the bits to be switched off
RETURNS: Nothing
CAUTION: LEDS must be connected to Port 2 and be active low
************************************************************************/
void LED_Off(uint8_t LED_bits);

/**
 * Will Set (to '1') the bits specified in bitMask (where
 * a '1' in the bitMask selects that bit) in port "port".
 * 
 * @Returns Nothing
 */
void Port_Set_bits(enum ENUM_PORTS port, uint8_t bitMask);

/**
 * Will clear (to '0') the bits specified in bitMask (where
 * a '1' in the bitMask selects that bit) in port "port".
 * 
 * @Returns Nothing
 */
void Port_Clear_bits(enum ENUM_PORTS port, uint8_t bitMask);

#endif /* _Outputs_H */
