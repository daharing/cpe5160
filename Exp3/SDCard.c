/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/

#include "SDCard.h"

#include <stdio.h>
#include "PORT.H"
#include "Main.h"

#include "SPI_Driver.h"
#include "Outputs.h"
#include "print_bytes.h"


/*-------------------------------------
|           PRIVATE STATICS            |
--------------------------------------*/

static uint8_t idata SD_Card_Type;


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

/**********************************************************************
DESC:    Sends the commands needed to initialize the SD card 
RETURNS: error flag
CAUTION:  
************************************************************************/
uint8_t SD_Card_init()
{
   /*-------------------------------------
   |              VARIABLES               |
   --------------------------------------*/

   uint8_t i, return_val, err_flag, spi_status;
   uint8_t byte_arry[8];
   uint32_t timeout,arg;
   
   
   /*-------------------------------------
   |           INITIALIZATIONS            |
   --------------------------------------*/

   printf("Initializing SD Card!\n\r");
   SD_Card_Type = SD_TYPE_UNK;
   return_val = SD_RETURN_SUCCESS;

   Port_Set_bits( PORT_1, nCS0_bit );


   /*-------------------------------------
   |         SEND 0XFF TEN TIMES          |
   --------------------------------------*/

   for (i = 0; i < 10; i++)
   {
      err_flag = SPI_Transfer(0xFF, &spi_status);
   }

   
   /*-------------------------------------
   |              SEND CMD0               |
   --------------------------------------*/

   printf("Sending CMD0 ...\n");
   Port_Clear_bits(PORT_1,nCS0_bit);
   err_flag = SD_Card_send_cmd(SD_CMD0, 0);

   if (err_flag == SD_RETURN_SUCCESS)
   {
      err_flag = SD_Card_recv_response(1, byte_arry);
      Port_Set_bits(PORT_1,nCS0_bit);
   }
   if (err_flag != SD_RETURN_SUCCESS)
   {
      Port_Set_bits(PORT_1,nCS0_bit);
      return_val = err_flag;
   }


   /*-------------------------------------
   |              SEND CMD8               |
   --------------------------------------*/

   if ( return_val == SD_RETURN_SUCCESS )
   {
      printf("Sending CMD8 ...\n");
      Port_Clear_bits(PORT_1, nCS0_bit);
      err_flag = SD_Card_send_cmd(SD_CMD8, 0x000001AA);

      if ( err_flag == SD_RETURN_SUCCESS )
      {
         err_flag = SD_Card_recv_response(5, byte_arry);
         Port_Set_bits(PORT_1, nCS0_bit);
         
         if ( err_flag != SD_RETURN_SUCCESS )
         {
            if (err_flag == SD_RETURN_ERR_RESPONSE && byte_arry[0] == 0x05)
            {
               return_val = SD_RETURN_SUCCESS;
               SD_Card_Type = SD_TYPE_STANDARD;
            }
            else
            {
               return_val = err_flag;
            }
         }
         else
         {
            if (byte_arry[4] != 0xAA)
            {
               err_flag = SD_RETURN_ERR_RESPONSE;
            }
            SD_Card_Type = SD_TYPE_VERSION_2;
         }
      }
      else
      {
         Port_Set_bits(PORT_1,nCS0_bit);
      }
   }


   /*-------------------------------------
   |              SEND CMD58              |
   --------------------------------------*/

   if (return_val == SD_RETURN_SUCCESS)
   {
      printf("Sending CMD58 ...\n");
      Port_Clear_bits(PORT_1,nCS0_bit);
      err_flag = SD_Card_send_cmd(SD_CMD58, 0);

      if (err_flag == SD_RETURN_SUCCESS)
      {
         err_flag = SD_Card_recv_response(5, byte_arry);
         Port_Set_bits(PORT_1,nCS0_bit);
         if ((byte_arry[2] & 0xFC) != 0xFC)
         {
            err_flag = SD_RETURN_ERR_VOLTAGE;
         }
      }
      else
      {
         Port_Set_bits(PORT_1,nCS0_bit);
      }
      if (err_flag != SD_RETURN_SUCCESS)
      {
         return_val = err_flag;
      }
   }

   if (return_val == SD_RETURN_SUCCESS)
   {
      if (SD_Card_Type == SD_TYPE_VERSION_2)
      {
         arg = 0x40000000;
      }
      else
      {
         arg = 0;
      }
      timeout = 0;
      printf("Sending ACMD41 ...\n");
      
      do
      {
         Port_Clear_bits(PORT_1,nCS0_bit);
         err_flag = SD_Card_send_cmd(SD_CMD55, 0);

         if (err_flag == SD_RETURN_SUCCESS)
         {
            err_flag = SD_Card_recv_response(1, byte_arry);
         }
         
         if ((byte_arry[0] == 0x01) || (byte_arry[0] == 0x00))
         {
            err_flag = SD_Card_send_cmd(SD_ACMD41, arg);
         }

         if (err_flag == SD_RETURN_SUCCESS)
         {
            SD_Card_recv_response(1, byte_arry);
         }

         Port_Set_bits(PORT_1,nCS0_bit);
         
         timeout++;
         
         if (timeout == 0)
         {
            err_flag = SD_RETURN_ERR_TIMEOUT;
         }

      } while (((byte_arry[0] & 0x01) == 0x01) && (err_flag == SD_RETURN_SUCCESS));

      if ( err_flag != SD_RETURN_SUCCESS )
      {
         Port_Set_bits(PORT_1,nCS0_bit);
         return_val = err_flag;
      }
   }

   if ( return_val == SD_RETURN_SUCCESS && SD_Card_Type == SD_TYPE_VERSION_2 )
   {
      printf("Sending CMD58 ...\n");
      Port_Clear_bits(PORT_1,nCS0_bit);
      err_flag = SD_Card_send_cmd(SD_CMD58, 0);
      if (err_flag == SD_RETURN_SUCCESS)
      {
         err_flag = SD_Card_recv_response(5, byte_arry);
         Port_Set_bits(PORT_1,nCS0_bit);
         if ((byte_arry[1] & 0x80) != 0x80)
         {
            err_flag = SD_RETURN_INACTIVE;
         }
         else
         {
            if ((byte_arry[1] & 0xC0) == 0xC0)
            {
               SD_Card_Type = SD_TYPE_HIGH_CAPACITY;
            }
            else
            {
               SD_Card_Type = SD_TYPE_STANDARD;
               printf("Sending CMD16 ...\n");
               Port_Clear_bits(PORT_1,nCS0_bit);
               err_flag = SD_Card_send_cmd(SD_CMD16, 512);

               if (err_flag == SD_RETURN_SUCCESS)
               {
                  err_flag = SD_Card_recv_response(1, byte_arry);
                  Port_Set_bits(PORT_1,nCS0_bit);
               }
            }
         }
      }
      else
      {
         Port_Set_bits(PORT_1,nCS0_bit);
      }
      if (err_flag != SD_RETURN_SUCCESS)
      {
         return_val = err_flag;
         SD_Card_print_error(return_val);
      }
   }


   /*-------------------------------------
   |     PRINT ANY ERRORS ENCOUNTERED     |
   --------------------------------------*/

   if (return_val != SD_RETURN_SUCCESS)
   {
      SD_Card_print_error(return_val);
   }

   return return_val;
}

uint8_t SD_Card_send_cmd(uint8_t command, uint32_t arg)
{
   /*-------------------------------------
   |              VARIABLES               |
   --------------------------------------*/

   uint8_t send_val, return_val, spi_status, err_flag;


   /*-------------------------------------
   |           INITIALIZATIONS            |
   --------------------------------------*/

   return_val = SD_RETURN_SUCCESS;
   

   /*-------------------------------------
   |            VERIFY COMMAND            |
   --------------------------------------*/

   if (command >= 64)
   {
      return SD_RETURN_ERR_ILLEGAL_CMD;
   }


   /*-------------------------------------
   |             SEND COMMAND             |
   --------------------------------------*/

   send_val = command | 0x40;
   err_flag = SPI_Transfer(send_val, &spi_status);

   if (err_flag == SD_RETURN_SUCCESS)
   {
      send_val = arg >> 24;
      err_flag = SPI_Transfer(send_val, &spi_status);
   }
   else
   {
      return_val = SD_RETURN_ERR_SPI;
   }
   if ( (return_val == SD_RETURN_SUCCESS) && (err_flag == SD_RETURN_SUCCESS) )
   {
      arg &= 0x00ffffff;
      send_val = arg >> 16;
      err_flag = SPI_Transfer(send_val, &spi_status);
   }
   else
   {
      return_val = SD_RETURN_ERR_SPI;
   }
   if ( (return_val == SD_RETURN_SUCCESS) && (err_flag == SD_RETURN_SUCCESS) )
   {
      arg &= 0x0000ffff;
      send_val = arg >> 8;
      err_flag = SPI_Transfer(send_val, &spi_status);
   }
   else
   {
      return_val = SD_RETURN_ERR_SPI;
   }
   if ((return_val == SD_RETURN_SUCCESS) && (err_flag == SD_RETURN_SUCCESS))
   {
      send_val = arg & 0x000000ff;
      err_flag = SPI_Transfer(send_val, &spi_status);
   }
   else
   {
      return_val = SD_RETURN_ERR_SPI;
   }

   if ( (return_val == SD_RETURN_SUCCESS) && (err_flag == SD_RETURN_SUCCESS) )
   {
      if (command == 0)
      {
         send_val = 0x95; /* CRC7 and end bit for CMD0 */
      }
      else if (command == 8)
      {
         send_val = 0x87; /* CRC7 and end bit for CMD8 */
      }
      else
      {
         send_val = 0x01; /* end bit only for other commands */
      }

      SPI_Transfer(send_val, &spi_status);
   }

   return return_val;
}

uint8_t SD_Card_recv_response(uint8_t recv_arry_sz, uint8_t *recv_arry)
{
   /*-------------------------------------
   |              VARIABLES               |
   --------------------------------------*/

   uint8_t i, return_val, err_flag, spi_status, timeout;


   /*-------------------------------------
   |           INITIALIZATIONS            |
   --------------------------------------*/

   return_val = SD_RETURN_SUCCESS;
   timeout = 0;
   i = 0;

   do
   {
      err_flag = SPI_Transfer(0xFF, &spi_status);
      
      if (err_flag != SD_RETURN_SUCCESS)
      {
         SPI_Transfer(0xFF, &spi_status);
         return SD_RETURN_ERR_SPI;
      }
      
      timeout++;
   } while( ((spi_status & 0x80) == 0x80) && (timeout != 0) );

   if ( timeout == 0 )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_TIMEOUT;
   }

   recv_arry[0] = spi_status;
   if ((spi_status == 0x00) || (spi_status == 0x01))
   {
      if (recv_arry_sz > 1)
      {
         for (i = 1; i < recv_arry_sz; i++)
         {
            SPI_Transfer(0xFF, &spi_status);
            recv_arry[i] = spi_status;
         }
      }
   }
   else
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_RESPONSE;
   }


   /*-------------------------------------
   |       SEND 8 EXTRA CLK CYCLES        |
   --------------------------------------*/

   SPI_Transfer(0xFF, &spi_status);

   return return_val;
}

uint8_t SD_Card_read_block(uint16_t recv_arry_sz, uint8_t *recv_arry)
{
   /*-------------------------------------
   |              VARIABLES               |
   --------------------------------------*/

   uint8_t err_flag, return_val, spi_status;
   uint16_t timeout,i;


   /*-------------------------------------
   |           INITIALIZATIONS            |
   --------------------------------------*/

   timeout = 0;
   i = 0;
   return_val = SD_RETURN_SUCCESS;
   

   /*-------------------------------------
   |             READ BLOCKS              |
   --------------------------------------*/

   do
   {
      err_flag = SPI_Transfer(0xFF, &spi_status);
      
      if (err_flag != SD_RETURN_SUCCESS)
      {
         SPI_Transfer(0xFF, &spi_status);
         return SD_RETURN_ERR_SPI;
      }
      
      timeout++;
   } while( ((spi_status & 0x80) == 0x80) && (timeout != 0) );

   if ( timeout == 0 )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_TIMEOUT;
   }

   if( spi_status != 0x00 )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_RESPONSE;
   }

   timeout = 0;
   do
   {
      err_flag = SPI_Transfer(0xFF, &spi_status);
      
      if ( err_flag != SD_RETURN_SUCCESS )
      {
         SPI_Transfer(0xFF, &spi_status);
         return SD_RETURN_ERR_SPI;
      }
      
      timeout++;
   } while( (spi_status == 0xFF) && (timeout != 0) );

   if ( timeout == 0 )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_TIMEOUT;
   }

   if( err_flag != SD_RETURN_SUCCESS )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_SPI;
   }

   if( spi_status != 0xFE )
   {
      SPI_Transfer(0xFF, &spi_status);
      return SD_RETURN_ERR_DATA;
   }

   for( i = 0; i < recv_arry_sz; i++ )
   {
      SPI_Transfer(0xFF, &spi_status);
      recv_arry[i] = spi_status;
   }


   /*-------------------------------------
   |          DISCARD CRC BYTES           |
   --------------------------------------*/

   SPI_Transfer(0xFF, &spi_status);	
   SPI_Transfer(0xFF, &spi_status);


   /*-------------------------------------
   |          SEND 8 CLK CYCLES           |
   --------------------------------------*/

   SPI_Transfer(0xFF,&spi_status);

   return return_val;
}

uint8_t SD_Card_get_type()
{
   return SD_Card_Type;
}

void SD_Card_print_error(uint8_t error_flag)
{
   switch (error_flag)
   {
   case SD_RETURN_ERR_TIMEOUT:
      printf("SD Timeout Error");
      break;
   case SD_RETURN_ERR_ILLEGAL_CMD:
      printf("Illegal Command\n\r");
      break;
   case SD_RETURN_ERR_RESPONSE:
      printf("SD_Card_recv_response Error");      
      break;
   case SD_RETURN_ERR_DATA:
      printf("Data Token Error");
      break;
   case SD_RETURN_ERR_VOLTAGE:
      printf("Incompatible Voltage");
      break;
   case SD_RETURN_INACTIVE:
      printf("Card is Inactive");
      break;
   case SD_RETURN_ERR_SPI:
      printf("SPI or Timeout Error");
      break;
   default:
      printf("Unknown Error");
      break;
   }
}
