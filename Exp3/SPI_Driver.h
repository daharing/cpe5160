/*-------------------------------------
|           PUBLIC INCLUDES            |
--------------------------------------*/

#include "Main.h"


/*-------------------------------------
|            PUBLIC DEFINES            |
--------------------------------------*/

/* Configurations */
#define CPOL                (0)     /* Clock Polarity: 0 = Clock high when idle, 1 = clock low when idle */
#define CPHA                (0)     /* Clock Phase: 0 = sample data then data shift, 1 = data shift then sample data */

/* Return Values */
#define SPI_RETURN_ERROR    (1)     /* Error return value */
#define SPI_RETURN_SUCCESS  (0)     /* No error return value */


/*-------------------------------------
|     PUBLIC FUNCTION DECLARATIONS     |
--------------------------------------*/

/**
 * Given a clock rate, initializes the SPI for master mode
 * 
 * @Returns SPI_RETURN_SUCCESS or SPI_RETURN_ERROR
 */
uint8_t SPI_Master_Init(uint32_t clock_rate);

/**
 * Transmits tx_value to SC slave and puts received byte
 * in rx_value.
 * 
 * @Returns SPI_RETURN_SUCCESS or SPI_RETURN_ERROR
 */
uint8_t SPI_Transfer(uint8_t tx_value, uint8_t *rx_value);
