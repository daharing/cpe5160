/*-------------------------------------
|           PRIVATE INCLUDES           |
--------------------------------------*/
#include "Main.h"
#include "PORT.H"
#include "I2C.h"
#include <stdio.h>

/*-------------------------------------
|           PRIVATE DEFINES            |
--------------------------------------*/
#define I2C_CLK_CONTINUE (0)
#define I2C_CLK_HALT 	 (1)

#define I2C_FREQ 	 	 (25000)
#define I2C_RELOAD 	 	 ( 65536 - (OSC_FREQ/(OSC_PER_INST * I2C_FREQ * 2UL)) )
#define I2C_RELOAD_H 	 (I2C_RELOAD/256)
#define I2C_RELOAD_L 	 (I2C_RELOAD%256)

/*-------------------------------------
|    PRIVATE FUNCTION DECLARATIONS     |
--------------------------------------*/
/**
 * Initialize and start timer 0 as I2C hardware
 * clock based on the I2C #define values above.
 */
void I2C_clock_start(void);

/**
 * Given I2C_CLK_CONTINUE as parameter, waits for timer
 * to timeout and restarts the timer. Else, waits for timer
 * timeout and returns. 
 */
void I2C_clock_delay(uint8_t control);


/*-------------------------------------
|         FUNCTION DEFINITIONS         |
--------------------------------------*/

void I2C_clock_start(void)
{
    /*-------------------------------------
    |         SETUP TIMER 1 MODE 1         |
    --------------------------------------*/
    TMOD &= 0xF0;
    TMOD |= 0x01;

    /*-------------------------------------
    |          DISABLE INTERRUPT           |
    --------------------------------------*/
    ET0 = 0;

    /*-------------------------------------
    |     LOAD I2C TIMER RELOAD VALUES     |
    --------------------------------------*/
    TH0 = I2C_RELOAD_H;
    TL0 = I2C_RELOAD_L;

    /*-------------------------------------
    |             START TIMER              |
    --------------------------------------*/
    TF0 = 0;
    TR0 = 1;
} /* I2C_clock_start() */

void I2C_clock_delay(uint8_t cntl)
{
    /*-------------------------------------
    |        WAIT FOR TIMER TIMEOUT        |
    --------------------------------------*/
    while ( TR0 == 1 && TF0 == 0 );

    /*-------------------------------------
    |              STOP TIMER              |
    --------------------------------------*/
    TR0 = 0;

    /*-------------------------------------
    |        RETURN TIMER IF NEEDED        |
    --------------------------------------*/
    if ( cntl != I2C_CLK_CONTINUE )
    {
        return;
    }

    /*-------------------------------------
    |        RELOAD AND START TIMER        |
    --------------------------------------*/
    TH0 = I2C_RELOAD_H;
    TL0 = I2C_RELOAD_L;

    TF0 = 0;
    TR0 = 1;

} /* I2C_clock_delay() */

uint8_t I2C_Read(uint8_t device_addr, uint32_t internal_addr, uint8_t internal_addr_sz, uint8_t nBytes, uint8_t *recv_arry)
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/
    uint8_t nBits;
    uint8_t byte_to_send;
    uint8_t send, sent, recv;
    uint8_t return_val;

    /*-------------------------------------
    |           INITIALIZATIONS            |
    --------------------------------------*/
    return_val = ERR_NONE;

    if (internal_addr_sz != 0)
    {
        return_val = I2C_Write(device_addr, internal_addr, internal_addr_sz, 0, recv_arry);

        if (return_val != ERR_NONE)
        {
            return return_val;
        }
    }

    /*-------------------------------------
    |           SET I2C SIGNALS            |
    --------------------------------------*/
    SDA = 1;
    SCL = 1;

    if ( SCL != 1 || SDA != 1 )
    {
        return ERR_BUS_BUSY;
    }

    /*-------------------------------------
    |            INITIALIZE I2C            |
    --------------------------------------*/
    I2C_clock_start();
    byte_to_send = device_addr << 1;
    byte_to_send |= 0x01; /* set LSB for read operation */

    SDA = 0; /* start condition */
    
    /*-------------------------------------
    |         SEND DEVICE ADDR BYTE        |
    --------------------------------------*/
    nBits = 8;
    do
    {
        nBits--;
        I2C_clock_delay(I2C_CLK_CONTINUE);

        /*-------------------------------------
        |               SEND BIT               |
        --------------------------------------*/
        SCL = 0;
        send = 0x01 & (byte_to_send >> nBits);
        SDA = (bit)send;

        /*-------------------------------------
        |             GET RESPONSE             |
        --------------------------------------*/
        I2C_clock_delay(I2C_CLK_CONTINUE);
        SCL = 1;
        while( SCL != 1 );

        /*-------------------------------------
        |             VERIFICATION             |
        --------------------------------------*/
        sent = SDA;
        if ( sent != send )
        {
            return_val = ERR_BUS_BUSY;
        }

    } while ( return_val == ERR_NONE && nBits != 0 );

    /*-------------------------------------
    |             GET RESPONSE             |
    --------------------------------------*/
    if ( return_val == ERR_NONE )
    {
        I2C_clock_delay(I2C_CLK_CONTINUE);
        SCL = 0;
        SDA = 1;
        I2C_clock_delay(I2C_CLK_CONTINUE);
        SCL = 1;
        while ( SCL != 1 );

        /*-------------------------------------
        |             VERIFICATION             |
        --------------------------------------*/
        sent = SDA;
        if ( sent != 0 )
        {
            return_val = ERR_NACK;
        }
    }

    /*-------------------------------------
    |             RECEIVE DATA             |
    --------------------------------------*/
    while ( return_val == ERR_NONE && nBytes != 0 )
    {
        /*-------------------------------------
        |             RECEIVE BYTE             |
        --------------------------------------*/
        nBits = 8;
        do
        {
            nBits--;
            I2C_clock_delay(I2C_CLK_CONTINUE);

            /* set signals */
            SCL = 0;
            SDA = 1;
            recv <<= 1;
            I2C_clock_delay(I2C_CLK_CONTINUE);
            SCL = 1;
            while ( SCL != 1 );

            sent = SDA;

            /* receive bit */
            recv |= sent;
        } while ( nBits != 0 );

        /*-------------------------------------
        |        SET AND INC ARRAY PTR         |
        --------------------------------------*/
        *recv_arry = recv;
        recv_arry++;

        /*-------------------------------------
        |    SIGNAL END/CONTINUATION OF TX     |
        --------------------------------------*/
        if (nBytes == 1)
        {
            send = 1; // NACK for last byte
        }
        else
        {
            send = 0; // ACK if more bytes are to be received
        }

        I2C_clock_delay(I2C_CLK_CONTINUE);

        /* set signals */
        SCL = 0;
        SDA = send;
        I2C_clock_delay(I2C_CLK_CONTINUE);
        SCL = 1;
        while (SCL != 1);

        nBytes--;
    }

    /*-------------------------------------
    |     SEND TX DONE AND STOP CLOCK      |
    --------------------------------------*/
    if (return_val != ERR_BUS_BUSY)
    {
        I2C_clock_delay(I2C_CLK_CONTINUE);
        /* set signals */
        SCL = 0;
        SDA = 0;
        I2C_clock_delay(I2C_CLK_CONTINUE);

        SCL = 1;
        while (SCL != 1);
        I2C_clock_delay(I2C_CLK_HALT);

        SDA = 1;
    }

    return return_val;
} /* I2C_Read() */

uint8_t I2C_Write(uint8_t device_addr, uint32_t internal_addr, uint8_t internal_addr_sz, uint8_t nBytes, uint8_t *send_arry)
{
    /*-------------------------------------
    |              VARIABLES               |
    --------------------------------------*/
    uint8_t byte_to_send;
    uint8_t send_addr_byte;
    uint8_t nBits;
    uint8_t send, sent;
    uint8_t return_val;

    /*-------------------------------------
    |           INITIALIZATIONS            |
    --------------------------------------*/
    return_val = ERR_NONE;
    send_addr_byte = internal_addr_sz;
    nBytes += 1; /* addr byte plus additional bytes to send added later */

    /*-------------------------------------
    |           SET I2C SIGNALS            |
    --------------------------------------*/
    SDA = 1;
    SCL = 1;

    /*-------------------------------------
    |            VERIFICATIONS             |
    --------------------------------------*/
    if ( SCL != 1 || SDA != 1 )
    {
        return ERR_BUS_BUSY;
    }

    /*-------------------------------------
    |           START I2C CLOCK            |
    --------------------------------------*/
    I2C_clock_start();


    /*-------------------------------------
    |         SET ADDR BYTE TO TX          |
    --------------------------------------*/
    byte_to_send = 0xFE & (device_addr << 1);

    /*-------------------------------------
    |           SIGNAL BEGIN TX            |
    --------------------------------------*/
    SDA = 0;

    /*-------------------------------------
    |              SEND BYTES              |
    --------------------------------------*/
    do
    {
        /*-------------------------------------
        |    LOOP ITERATION INITIALIZATIONS    |
        --------------------------------------*/
        nBits = 8;

        /*-------------------------------------
        |           SEND SINGLE BYTE           |
        --------------------------------------*/
        do
        {
            nBits--;
            I2C_clock_delay(I2C_CLK_CONTINUE);

            /* Send bit */
            SCL = 0;
            send = 0x01 & (byte_to_send >> nBits);
            SDA = (bit)send;//equaivalent to SDA = send & 0x01 ?

            /*-------------------------------------
            |             GET RESPONSE             |
            --------------------------------------*/
            I2C_clock_delay(I2C_CLK_CONTINUE);
            SCL = 1;
            while ( SCL != 1 );

            /*-------------------------------------
            |             VERIFICATION             |
            --------------------------------------*/
            sent = SDA;
            if (sent != send)
            {
                return_val = ERR_BUS_BUSY;
            }
        } while ( return_val == ERR_NONE && nBits != 0 );
        

        /*-------------------------------------
        |           TX VERIFICATION            |
        --------------------------------------*/
        if (return_val != ERR_NONE)
        {
            continue;
        }

        /*-------------------------------------
        |      CONTINUE CLK, WAIT FOR ACK      |
        --------------------------------------*/
        I2C_clock_delay(I2C_CLK_CONTINUE);
        SCL = 0;
        SDA = 1; /* Allow slave to tx ACK */

        I2C_clock_delay(I2C_CLK_CONTINUE);

        /*-------------------------------------
        |             GET RESPONSE             |
        --------------------------------------*/
        SCL = 1;
        while (SCL != 1);

        /*-------------------------------------
        |             VERIFICATION             |
        --------------------------------------*/
        sent = SDA;
        if ( sent != 0 )
        {
            return_val = ERR_NACK;
        }

        /*-------------------------------------
        |      DETERMINE NEXT DATA TO TX       |
        --------------------------------------*/
        if ( send_addr_byte == 0 )
        {
            nBytes -= 1;
            byte_to_send = send_arry[0];
            send_arry++;
        }
        else
        {
            send_addr_byte -= 1;
            byte_to_send = ( internal_addr >> (send_addr_byte*8) );
        }

    } while ( return_val == ERR_NONE && nBytes != 0 );

    /*-------------------------------------
    |           SIGNAL END OF TX           |
    --------------------------------------*/
    if ( return_val == ERR_BUS_BUSY )
    {
        return return_val;
    }

    I2C_clock_delay(I2C_CLK_CONTINUE);
    SCL = 0;
    SDA = 0;

    /*-------------------------------------
    |             WAIT FOR ACK             |
    --------------------------------------*/
    I2C_clock_delay(I2C_CLK_CONTINUE);
    SCL = 1;
    while (SCL != 1);

    I2C_clock_delay(I2C_CLK_HALT);
    SDA = 1;

    return return_val;
} /* I2C_Write() */
