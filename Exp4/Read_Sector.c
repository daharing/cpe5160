#include "Main.H"
#include "PORT.H"
#include "SDCard.h"
#include "Read_Sector.h"
#include "Outputs.h"





uint8_t Read_Sector(uint32_t sector_number, uint16_t sector_size, uint8_t * array_for_data)
{
	uint8_t SDtype,error_flag=No_Disk_Error;   

	SDtype=SD_Card_get_type();
    Port_Clear_bits(PORT_1, nCS0_bit);
    error_flag=SD_Card_send_cmd(17,(sector_number<<SDtype));
    if(error_flag==SD_RETURN_SUCCESS) error_flag=SD_Card_read_block(sector_size,array_for_data);
    Port_Set_bits(PORT_1, nCS0_bit);

	if(error_flag!=SD_RETURN_SUCCESS)
	{
       error_flag=Disk_Error;
    }
    return error_flag;
}
