#include "main.h"

#include "exp_4_fall2019.h"
#include "Outputs.h"
#include "PORT.H"
#include "Timer0_hardware_delay_1ms.h"
#include <stdio.h>

/*-------------------------------------
|    EXTERNAL FUNCTION DECLARATIONS    |
--------------------------------------*/

extern int8_t exp3_init();
extern int8_t exp3_execute();
extern void   exp3_failure();


/*-------------------------------------
|         PROGRAM ENTRY POINT          |
--------------------------------------*/

main()
{
    /*-------------------------------------
    |              INITIALIZE              |
    --------------------------------------*/

    LED_On(Amber_LED);
    
    if ( RETURN_SUCCESS != exp3_init() )
    {
        printf("Failed to initialize...\n\r");
        exp3_failure();
    }
    
    Timer0_DELAY_1ms(500); /* Delay to see Amber LED (visual indication program is executing) */

    LED_Off(Amber_LED);


    /*-------------------------------------
    |               EXECUTE                |
    --------------------------------------*/

    LED_On(Green_LED);

    exp3_execute();

    LED_Off(Green_LED);


    /*-------------------------------------
    |                ERROR                 |
    --------------------------------------*/

    exp3_failure();

} /* main() */
