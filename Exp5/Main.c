#include "main.h"

#include "exp_5_fall2019.h"
#include "Outputs.h"
#include "PORT.H"
#include <stdio.h>

/*-------------------------------------
|    EXTERNAL FUNCTION DECLARATIONS    |
--------------------------------------*/

extern int8_t exp5_init();
extern int8_t exp5_execute();
extern void   exp5_failure();


/*-------------------------------------
|         PROGRAM ENTRY POINT          |
--------------------------------------*/

main()
{
    /*-------------------------------------
    |              INITIALIZE              |
    --------------------------------------*/

    LED_On(Amber_LED);
    
    if ( RETURN_SUCCESS != exp5_init() )
    {
        // printf("Failed to initialize...\n\r");
        exp5_failure();
    }
    
    // Timer0_DELAY_1ms(500); /* Delay to see Amber LED (visual indication program is executing) */

    LED_Off(Amber_LED);


    /*-------------------------------------
    |               EXECUTE                |
    --------------------------------------*/

    // LED_On(Green_LED);

    exp5_execute();

    // LED_Off(Green_LED);


    /*-------------------------------------
    |                ERROR                 |
    --------------------------------------*/

    exp5_failure();

} /* main() */
