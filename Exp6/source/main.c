#include "AT89C51RC2.h"
#include "stdio.h"
#include "main.h"
#include "PORT.H"
#include "UART.h"
#include "SPI.h"
#include "SDcard.h"
#include "Long_Serial_In.h"
#include "File_System_struct.h"
#include "Directory_Functions_struct.h"
#include "LCD_routines.h"
#include "LED_Control.h"
#include "Outputs.h"
#include "print_bytes.h"
#include "STA013_Config.h"
#include "song_player.h"



uint8_t xdata buf1[512];
uint8_t xdata buf2[512];

uint8_t code SD_Card_string[]="SD Card...";
uint8_t code High_Cap_string[]="HC";
uint8_t code Standard_Cap_string[]="SC";
uint8_t code Drive_string[]="Drive...";
uint8_t code Root_string[]="Root";
uint8_t code SDSC[]="Std. Capacity";
uint8_t code SDHC[]="High Capacity";


main()
{
  uint32_t Current_directory, Entry_clus;
  uint16_t i, num_entries, entry_num;
  uint8_t error_flag, SD_stat;
  FS_values_t * Drive_p;


  LEDS_OFF(Amber_LED|Yellow_LED|Green_LED);
  LEDS_ON(Red_LED);
  i=0;
  while(i<=60000) i++;
  LEDS_OFF(Red_LED);
  AUXR=0x0c;   // make all of XRAM available
  if(OSC_PER_INST==6)
  {
    CKCON0=0x01;  // set X2 clock mode
  }
  else if(OSC_PER_INST==12)
  {
    CKCON0=0x00;  // set standard clock mode
  }
  uart_init(9600);
  LCD_Init();
  printf("File System Test Program\n\r\n\n");
  error_flag=SPI_Master_Init(200000UL);
  if(error_flag!=no_errors)
  {
    LEDS_ON(Red_LED);
    while(1);
  }
  printf("SD Card Initialization ... \n\r");
  LCD_Print(line1,10,SD_Card_string);
  error_flag=SD_card_init();
  if(error_flag!=no_errors)
  {
    LEDS_ON(Red_LED);
    print_error(error_flag);
    while(1);
  }
  error_flag=Return_SD_Card_Type();
  if(error_flag==Standard_Capacity)
  {
    LCD_Print(no_addr_change,2,Standard_Cap_string);
  }
  else
  {
    LCD_Print(no_addr_change,2,High_Cap_string);
  }
  error_flag=SPI_Master_Init(20000000UL);
  if(error_flag!=no_errors)
  {
    LEDS_ON(Red_LED);
    while(1);
  }
  SD_stat=Return_SD_Card_Type();
  if(SD_stat==Standard_Capacity)
  {
    LCD_Print(line2,0,SDSC);
  }
  else if(SD_stat==High_Capacity)
  {
    LCD_Print(line2,0,SDHC);
  }
  LCD_Print(line2,8,Drive_string);

  STA013_init();        //Configure the STA013 MP3 Decoder

  for(i=0;i<512;i++)
  {
    buf1[i]=0xff;  // erase valout for debug
    buf2[i]=0xff;
  }
  error_flag=Mount_Drive(buf1);
  if(error_flag!=no_errors)
  {
    LEDS_ON(Red_LED);
    while(1);
  }
  LCD_Print(no_addr_change,4,Root_string);
  Drive_p=Export_Drive_values();
  Current_directory=Drive_p->FirstRootDirSec;


  // Main Loop

  while(1)
  {
    printf("Directory Sector = %lu\n\r",Current_directory);
    num_entries=Print_Directory(Current_directory, buf1);
    printf("Enter Selection = ");
    entry_num=(uint16_t)long_serial_input();
    if(entry_num<=num_entries)
    {
      Entry_clus=Read_Dir_Entry(Current_directory, entry_num, buf1);
      printf("Entry_clus = %lu\n\r",Entry_clus);
      if((Entry_clus&directory_bit)==directory_bit)
      {
        Entry_clus&=0x0FFFFFFF;
        Current_directory=First_Sector(Entry_clus);
      }
      else
      {
        play_song(Entry_clus);
      }

    }
    else
    {
      printf("Invalid Selection\n\r");
    }
  }
}
