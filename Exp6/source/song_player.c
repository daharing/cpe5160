#include "stdio.h"
#include "main.h"
#include "PORT.H"
#include "SPI.h"
#include "File_System_struct.h"
#include "Directory_Functions_struct.h"
#include "Read_Sector.h"
#include "Outputs.h"
#include "LED_Control.h"
#include "song_player.h"

#define EOF_MARKER (0x0FFFFFFF)
#define END_OF_SONG (0)
#define MORE_SONG   (1)

#define BUFFER_SIZE (512)
extern uint8_t xdata buf1[BUFFER_SIZE];
extern uint8_t xdata buf2[BUFFER_SIZE];

// LED stage
#define LOADING_BUFFER_1 (1)
#define LOADING_BUFFER_2 (2)
#define SENDING_BUFFER_1 (3)
#define SENDING_BUFFER_2 (4)

#define BUFFER_ACTIVE   (1)
#define BUFFER_INACTIVE (0)


static uint32_t sector, sector_offset, current_cluster;

uint8_t read_song_sector(uint8_t *buf)
{
  if(sector_offset == Export_Drive_values()->SecPerClus)
  {
    current_cluster = Find_Next_Clus(current_cluster, buf);
    sector_offset = 0;
  }
  if(sector_offset == 0) sector = First_Sector(current_cluster);
  Read_Sector((sector_offset + sector), Export_Drive_values()->BytesPerSec, buf);
  sector_offset++;
  if(current_cluster == EOF_MARKER) return END_OF_SONG;
  return MORE_SONG;
}

void show_stage_LED(uint8_t n)
{
  LEDS_OFF(Red_LED|Amber_LED|Yellow_LED|Green_LED);
  if(n&(1<<0)) LEDS_ON(Red_LED);
  if(n&(1<<1)) LEDS_ON(Amber_LED);
  if(n&(1<<2)) LEDS_ON(Yellow_LED);
  if(n&(1<<3)) LEDS_ON(Green_LED);
}

void play_song(uint32_t Start_Cluster)
{
  uint16_t index1, index2;
  uint8_t buffer1, buffer2, temp8;
  uint8_t song_status = MORE_SONG;
  current_cluster = Start_Cluster;

  sector = First_Sector(Start_Cluster);
  sector_offset=0;
  buffer1=BUFFER_ACTIVE;
  buffer2=BUFFER_INACTIVE;

  index1=0;
  read_song_sector(buf1);
  show_stage_LED(LOADING_BUFFER_1);

  index2=0;
  read_song_sector(buf2);
  show_stage_LED(LOADING_BUFFER_2);

  do
  {
    do
    {
      if(DATA_REQ==0)
      {
        show_stage_LED(SENDING_BUFFER_1);
        P1_set_bit(BIT_EN_bit);
        SPI_Transfer(buf1[index1], &temp8);
        P1_clear_bit(BIT_EN_bit);
        index1++;


        if(index1>BUFFER_SIZE-1)
        {
          if(index2>BUFFER_SIZE-1)
          {
            index2=0;
            show_stage_LED(LOADING_BUFFER_2);
            song_status = read_song_sector(buf2);
          }

          buffer1=BUFFER_INACTIVE;
          buffer2=BUFFER_ACTIVE;
        }
      }
      else
      {
        if(index2>BUFFER_SIZE-1)
        {
          index2=0;
          song_status = read_song_sector(buf2);
          show_stage_LED(LOADING_BUFFER_2);
        }
        else
        {
          if(index1>BUFFER_SIZE-1)
          {
            buffer1=BUFFER_INACTIVE;
            buffer2=BUFFER_ACTIVE;
          }
        }
      }
    }while(buffer1==BUFFER_ACTIVE);
    do
    {
      if(DATA_REQ==0)
      {
        show_stage_LED(SENDING_BUFFER_2);
        P1_set_bit(BIT_EN_bit);
        SPI_Transfer(buf2[index2], &temp8);
        P1_clear_bit(BIT_EN_bit);
        index2++;
        if(index2>BUFFER_SIZE-1)
        {
          if(index1>BUFFER_SIZE-1)
          {
            index1=0;
            show_stage_LED(LOADING_BUFFER_1);
            song_status = read_song_sector(buf1);
          }
          buffer2=BUFFER_INACTIVE;
          buffer1=BUFFER_ACTIVE;
        }
      }
      else
      {
        if(index1>BUFFER_SIZE-1)
        {
          index1=0;
          show_stage_LED(LOADING_BUFFER_1);
          song_status = read_song_sector(buf1);
        }
        else
        {
          if(index2>BUFFER_SIZE-1)
          {
            buffer2=BUFFER_INACTIVE;
            buffer1=BUFFER_ACTIVE;
          }
        }
      }
    }while(buffer2==BUFFER_ACTIVE);
  }while(song_status == MORE_SONG);
}
